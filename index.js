const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const cors = require('cors');
const port = 8000;
app.use(cors());
const password = 'password';
app.use(express.json());
let crypt;
app.post('/encode/:code/', (req, res) => {
    crypt = Vigenere.Cipher(req.body.pass || password).crypt(req.params.code);
    res.send(crypt)
});

app.post('/decode/:code/', (req, res) => {
    crypt = Vigenere.Decipher(req.body.pass || password).crypt(req.params.code);
    res.send(crypt)
});
app.get('/*', (req, res) => {
    res.send("Необходим GET запрос с /encode/* или /decode/*")
});
app.listen(port, () => {
    console.log('We are live on 8000 port')
});